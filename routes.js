const http = require("http");

http.createServer(function(request,response){

	response.writeHead(200,{'Content-Type':'text/plain'});
	response.end("Hello from our First NodeJS Server!");

}).listen(3000);

console.log("Server is running on localhost:3000!");
